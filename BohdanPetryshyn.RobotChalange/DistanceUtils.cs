﻿using Robot.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BohdanPetryshyn.RobotChalange
{
    public class DistanceUtils
    {
        public static int Cost(Position pos1, Position pos2)
        {
            return SquareInt(pos2.X - pos1.X) + SquareInt(pos2.Y - pos1.Y);
        }

        public static int Cost(IList<Position> path)
        {
            Position lastPosition = null;
            int cost = 0;
            foreach (var position in path)
            {
                if (lastPosition == null) lastPosition = position;

                cost += Cost(lastPosition, position);
                lastPosition = position;
            }
            return cost;
        }

        private static int SquareInt(int num)
        {
            return (int) Math.Pow(num, 2);
        }
    }
}
