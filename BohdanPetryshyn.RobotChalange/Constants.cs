﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BohdanPetryshyn.RobotChalange
{
    public class Constants
    {
        public static readonly int MIN_ENERGY_FOR_CREATING_NEW_ROBOT = 300;

        public static readonly int ROBOT_CREATION_ROUND_TRESHOLD = 45;

        public static readonly int CREATED_ROBOT_ENERGY = 100;

        public static readonly int ENERGY_COLLECTION_RADIUS = 2;

        public static readonly int MAX_STEP_SIZE = 3;

        public static readonly int STATION_REOCCUPATION_ROUND_TRESHOLD = 0;

        public static readonly String AUTHOR_NAME = "Bohdan Petryshyn";
    }
}
