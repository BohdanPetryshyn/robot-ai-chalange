﻿using Robot.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BohdanPetryshyn.RobotChalange
{
    public class StationsController
    {
        private ISet<EnergyStation> stations;

        private ISet<Position> occupiedStationPositions = new HashSet<Position>();

        private Map map;

        public StationsController(Map map)
        {
            stations = new HashSet<EnergyStation>(map.Stations);
            this.map = map;
        }

        public void UpdateOccupiedStations(IList<Robot.Common.Robot> robots, bool ignoreEnemies)
        {
            foreach(var robot in robots)
            {
                var occupiedStationsByRobot = map.GetNearbyResources(robot.Position, Constants.ENERGY_COLLECTION_RADIUS);
                var occupiedStationPositionsByRobot = occupiedStationsByRobot.Select(station => station.Position);
                if(!(IsEnemy(robot) && ignoreEnemies))
                {
                    occupiedStationPositions.UnionWith(occupiedStationPositionsByRobot);
                }
            }
        }

        private bool IsEnemy(Robot.Common.Robot robot)
        {
            return robot.OwnerName != Constants.AUTHOR_NAME;
        }

        public EnergyStation GetNearestNotOccupiedStation(Robot.Common.Robot robot)
        {
            var sortedStations = SortStationsByRangeTo(robot);
            return sortedStations.FirstOrDefault(IsNotOccupied);
        }

        public bool IsNotOccupied(EnergyStation station)
        {
            return !IsOccupied(station);
        }

        public bool IsOccupied(EnergyStation station)
        {
            return occupiedStationPositions.Contains(station.Position);
        }

        public bool NotOccupiedStationExists()
        {
            return occupiedStationPositions.Count < stations.Count;
        }

        public void OccupyStation(EnergyStation station)
        {
            var nearbyStations = map.GetNearbyResources(station.Position, Constants.ENERGY_COLLECTION_RADIUS);
            var nearbyStationPositions = nearbyStations.Select(s => s.Position);
            occupiedStationPositions.UnionWith(nearbyStationPositions);
        }

        public void UnoccupyStation(EnergyStation station)
        {
            this.occupiedStationPositions.Remove(station.Position);
        }

        private IEnumerable<EnergyStation> SortStationsByRangeTo(Robot.Common.Robot robot)
        {
            return stations.OrderBy(station => DistanceUtils.Cost(robot.Position, station.Position));
        }
    }
}
