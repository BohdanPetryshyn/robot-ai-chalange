﻿using System;
using System.Collections.Generic;
using System.Linq;
using Robot.Common;

namespace BohdanPetryshyn.RobotChalange
{
    public class BohdanPetryshynRobotAlgorithm : IRobotAlgorithm { 

        private StationsController stationsController = null;

        private MovementController movementController = new MovementController();

        private int roundCount = 0;

        public BohdanPetryshynRobotAlgorithm()
        {
            Logger.OnLogRound += (s, e) => roundCount++;
        }

        public string Author => Constants.AUTHOR_NAME;

        public RobotCommand DoStep(IList<Robot.Common.Robot> robots, int robotToMoveIndex, Map map)
        {
            Initialize(map);
            stationsController.UpdateOccupiedStations(robots, ShouldReoccupyEnemyStations());

            var robot = robots[robotToMoveIndex];

            if (ShouldCreateNewRobot(robot))
            {
                return new CreateNewRobotCommand();
            }

            if (ShouldCollectEnergy(map, robots, robotToMoveIndex))
            {
                return new CollectEnergyCommand();
            }

            return new MoveCommand() { NewPosition = DoMove(robots, robotToMoveIndex) };
        }

        private bool ShouldReoccupyEnemyStations()
        {
            return roundCount < Constants.STATION_REOCCUPATION_ROUND_TRESHOLD;
        }

        private bool ShouldCreateNewRobot(Robot.Common.Robot robot)
        {
            return robot.Energy >= Constants.MIN_ENERGY_FOR_CREATING_NEW_ROBOT
                   && stationsController.NotOccupiedStationExists()
                   && PathToStationPossible(robot)
                   && roundCount < Constants.ROBOT_CREATION_ROUND_TRESHOLD;
        }

        private bool PathToStationPossible(Robot.Common.Robot robot)
        {
            var nearestStation = stationsController.GetNearestNotOccupiedStation(robot);
            if (nearestStation == null) return false;
            var path = movementController.GeneratePath(robot.Position, nearestStation.Position);

            return DistanceUtils.Cost(path) < Constants.CREATED_ROBOT_ENERGY;
        }

        private bool ShouldCollectEnergy(Map map, IList<Robot.Common.Robot> robots, int robotIndex)
        {
            var avalibleStations = map.GetNearbyResources(robots[robotIndex].Position, Constants.ENERGY_COLLECTION_RADIUS);

            if (avalibleStations == null) return false;

            var nearNotOccupiedStation = avalibleStations.Any(stationsController.IsNotOccupied);
            var nearSelfOccupiedStation = avalibleStations.Any(station => movementController.IsRobotDestination(robotIndex, station.Position));

            return (nearNotOccupiedStation || nearSelfOccupiedStation);
        }

        private Position DoMove(IList<Robot.Common.Robot> robots, int robotIndex)
        {
            if (!movementController.HasDestination(robotIndex))
            {
                var bestStation = stationsController.GetNearestNotOccupiedStation(robots[robotIndex]);
                stationsController.OccupyStation(bestStation);
                movementController.SetRobotDestination(robotIndex, bestStation.Position);
            }
            
            return movementController.NextStep(robots, robotIndex);
        }

        private void Initialize(Map map)
        {
            if(stationsController == null)
            {
                stationsController = new StationsController(map);
            }
        }
    }
}
