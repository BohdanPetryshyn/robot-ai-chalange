﻿using Robot.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BohdanPetryshyn.RobotChalange
{
    public class MovementController
    { 
        private IDictionary<int, Position> robotsDestinations = 
            new Dictionary<int, Position>();

        public void SetRobotDestination(int robotIndex, Position destination)
        {
            robotsDestinations.Add(robotIndex, destination);
        }

        public Position GetRobotDestination(int robotIndex)
        {
            return robotsDestinations[robotIndex];
        }

        public bool IsRobotDestination(int robotIndex, Position destination)
        {
            if (!robotsDestinations.ContainsKey(robotIndex)) return false;
            return robotsDestinations[robotIndex] == destination;
        }

        public Position NextStep(IList<Robot.Common.Robot> robots, int robotIndex)
        {
            var robot = robots[robotIndex];

            var destination = robotsDestinations[robotIndex];
            var robotPosition = robot.Position;

            return NextStep(robotPosition, destination);
        }

        private Position NextStep(Position position, Position destination)
        {
            var dX = destination.X - position.X;
            var dY = destination.Y - position.Y;

            var xStep = Math.Abs(dX) < Constants.MAX_STEP_SIZE ? dX : Math.Sign(dX) * Constants.MAX_STEP_SIZE;
            var yStep = Math.Abs(dY) < Constants.MAX_STEP_SIZE ? dY : Math.Sign(dY) * Constants.MAX_STEP_SIZE;

            return new Position(position.X + xStep,
                position.Y + yStep);
        }

        public bool HasDestination(int robotIndex)
        {
            return robotsDestinations.ContainsKey(robotIndex);
        }

        public IList<Position> GeneratePath(Position p1, Position p2)
        {
            IList<Position> path = new List<Position> { p1 };

            for (Position p = p1.Copy(); p != p2; p = NextStep(p, p2))
            {
                path.Add(p);
            }

            path.Add(p2);
            return path;
        }
    }
}
