﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Robot.Common;

namespace BohdanPetryshyn.RobotChalange.Test
{
    [TestClass]
    public class MovementControllerTest
    {
        private MovementController movementController;

        private Robot.Common.Robot robot;

        private IList<Robot.Common.Robot> robots;

        private static readonly int ROBOT_INDEX = 0;

        private static readonly Position ROBOT_POSITION = new Position(3, 1);

        private static readonly Position NEAR_DESTINATION = new Position(3, 1 + Constants.MAX_STEP_SIZE);

        private static readonly Position FAR_DESTINATION = new Position(3, 30);

        [TestInitialize]
        public void Init()
        {
            movementController = new MovementController();

            robot = new Robot.Common.Robot() { Energy = 100, Position = ROBOT_POSITION, OwnerName = "Bohdan Petryshyn" };

            robots = new List<Robot.Common.Robot>() { robot };
        }

        [TestMethod]
        public void NextStepShouldBeDestinationIfRobotIsOneStepFarFromIt()
        {
            movementController.SetRobotDestination(ROBOT_INDEX, NEAR_DESTINATION);
            Position expectedResult = NEAR_DESTINATION;

            Position result = movementController.NextStep(robots, ROBOT_INDEX);

            Assert.AreEqual(result, expectedResult);
        }

        [TestMethod]
        public void NextStepShouldBeOneStepCloserToDestinationIfRobotFarFromIt()
        {
            movementController.SetRobotDestination(ROBOT_INDEX, FAR_DESTINATION);
            Position expectedResult = new Position(ROBOT_POSITION.X, ROBOT_POSITION.Y + Constants.MAX_STEP_SIZE);

            Position result = movementController.NextStep(robots, ROBOT_INDEX);

            Assert.AreEqual(result, expectedResult);
        }

        [TestMethod]
        public void NextStepShouldBeOnTheSamePositionIfRobotOnTheDestination()
        {
            movementController.SetRobotDestination(ROBOT_INDEX, ROBOT_POSITION);
            Position expectedResult = ROBOT_POSITION;

            Position result = movementController.NextStep(robots, ROBOT_INDEX);

            Assert.AreEqual(result, expectedResult);
        }
    }
}
