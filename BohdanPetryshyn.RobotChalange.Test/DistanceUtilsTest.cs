﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Robot.Common;
using BohdanPetryshyn.RobotChalange;
using System.Collections.Generic;

namespace BohdanPetryshyn.RobotChalange.Test
{
    [TestClass]
    public class DistanceUtilsTest
    {
        [TestMethod]
        public void CostShouldReturn8IfStartIs1_1AndDestinationIs3_3()
        {
            Position position1 = new Position(1, 1);
            Position position2 = new Position(3, 3);
            int expectedResult = 8;

            int result = DistanceUtils.Cost(position1, position2);

            Assert.AreEqual(expectedResult, result);
        }

        [TestMethod]
        public void CostShouldReturn17IfPathIs1_1To3_3To3_6()
        {
            IList<Position> path = new List<Position>()
            {
                new Position(1, 1),
                new Position(3, 3),
                new Position(3, 6)
            };
            int expectedResult = 17;

            int result = DistanceUtils.Cost(path);

            Assert.AreEqual(expectedResult, result);
        }

        [TestMethod]
        public void CostShouldReturn0IfPathIsEmpty()
        {
            IList<Position> path = new List<Position>();
            int expectedResult = 0;

            int result = DistanceUtils.Cost(path);

            Assert.AreEqual(expectedResult, result);
        }
    }
}
