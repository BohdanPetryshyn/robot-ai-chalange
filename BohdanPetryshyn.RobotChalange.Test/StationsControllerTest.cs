﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Robot.Common;

namespace BohdanPetryshyn.RobotChalange.Test
{
    [TestClass]
    public class StationsControllerTest
    {
        private Map map;

        private StationsController stationsController;

        private Robot.Common.Robot robot;

        private EnergyStation station1;

        private EnergyStation station2;

        private EnergyStation nearestStation;

        private static readonly Position ROBOT_POSITION = new Position(2, 2);

        private static readonly Position STATION_1_POSITION= new Position(10, 20);

        private static readonly Position STATION_2_POSITION = new Position(25, 25);

        private static readonly Position NEAREST_STATION_POSITION = new Position(ROBOT_POSITION.X + 1, ROBOT_POSITION.Y);

        private static readonly Position OTHER_STATION_POSITION = new Position(35, 76);

        [TestInitialize]
        public void Init()
        {
            station2 = CreateStationOnPosition(STATION_2_POSITION);
            station1 = CreateStationOnPosition(STATION_1_POSITION);
            nearestStation = CreateStationOnPosition(NEAREST_STATION_POSITION);

            map = new Map();
            map.Stations.Add(station2);
            map.Stations.Add(station1);
            map.Stations.Add(nearestStation);

            stationsController = new StationsController(map);

            robot = CreateRobotOnPosition(ROBOT_POSITION);
        }

        [TestMethod]
        public void GetNearestNotOccupiedStationShouldReturnStationIfNotOccupiedStationExists()
        {
            OccupyStations(station1, nearestStation);
            EnergyStation expectedResult = station2;

            EnergyStation result = stationsController.GetNearestNotOccupiedStation(robot);

            Assert.AreEqual(expectedResult, result);
        }

        [TestMethod]
        public void GetNearestNotOccupiedStationShouldReturnNullIfNotOccupiedStationDoesNotExist()
        {
            OccupyStations(station1, station2, nearestStation);
            EnergyStation expectedResult = null;

            EnergyStation result = stationsController.GetNearestNotOccupiedStation(robot);

            Assert.AreEqual(expectedResult, result);
        }

        [TestMethod]
        public void GetNearestNotOccupiedStationShouldReturnNearestNotOccupiedStationIfThereAreALot()
        {
            EnergyStation expectedResult = nearestStation;

            EnergyStation result = stationsController.GetNearestNotOccupiedStation(robot);

            Assert.AreEqual(nearestStation, result);
        }

        [TestMethod]
        public void UpdateOccupiedStationsShouldMakeStationsNearEnemiesOccupied()
        {
            var enemies = new List<Robot.Common.Robot>() {
                CreateEnemyRobotOnPosition(STATION_1_POSITION)
            };

            stationsController.UpdateOccupiedStations(enemies, false);

            Assert.IsTrue(stationsController.IsOccupied(station1));
        }

        [TestMethod]
        public void UpdateOccupiedStationsShouldNotMakeStationsFarFromEnemiesOccupied()
        {
            var enemies = new List<Robot.Common.Robot>() {
                CreateEnemyRobotOnPosition(STATION_1_POSITION),
                CreateEnemyRobotOnPosition(OTHER_STATION_POSITION)
            };

            stationsController.UpdateOccupiedStations(enemies, false);

            Assert.IsTrue(stationsController.IsNotOccupied(station2));
        }

        [TestMethod]
        public void UpdateOccupiedStationsShouldIgnoreEnemiesIfConsiderEnemiesIsFals()
        {
            var enemies = new List<Robot.Common.Robot>() {
                CreateEnemyRobotOnPosition(STATION_1_POSITION)
            };

            stationsController.UpdateOccupiedStations(enemies, true);

            Assert.IsTrue(stationsController.IsNotOccupied(station1));
        }

        private static EnergyStation CreateStationOnPosition(Position position)
        {
            return new EnergyStation() { Energy = 100, Position = position, RecoveryRate = 20 };
        }

        private static EnergyStation CreateStationOnPosition(int x, int y)
        {
            return CreateStationOnPosition(new Position(x, y));
        }

        private static Robot.Common.Robot CreateRobotOnPosition(Position position)
        {
            return new Robot.Common.Robot() { Energy = 100, Position = position, OwnerName = "Bohdan Petryshyn" };
        }

        private static Robot.Common.Robot CreateEnemyRobotOnPosition(Position position)
        {
            return new Robot.Common.Robot() { Energy = 100, Position = position, OwnerName = "Some super-evil enemy" };
        }

        private void OccupyStations(params EnergyStation[] stations)
        {
            foreach(var station in stations)
            {
                stationsController.OccupyStation(station);
            }
        }
    }
}
